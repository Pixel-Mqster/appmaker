# Appmaker

### Create an app from an Binary!

**Usage: `appmaker [script] [--options]`**

### Options:
   

`-n` or `--name` for the name of the app

`-i` or `--icon` for an icon file (must be .icns)
   
`-a` or `--author` for an author
   
`-v` or `--version` for a version number (must be a string)

### Example: 

`appmaker ./main.py --name "My App" --icon "./assets/icon.icns" --author "Pixel Master" --version "1.0"
`
 
`appmaker -h` or `appmaker --help` for help
